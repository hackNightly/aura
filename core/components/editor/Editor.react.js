'use strict';

import { Document } from '../document/Document.react.js';

let shortid = require('shortid');

require('../editor/editor.css');

export default class Editor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      documents: [{statements: []}]
    };
  }

  render() {
    return (
      <div id='aura-editor'>
        {this.state.documents.map((doc) => {
          return <Document initialDocument={doc} key={shortid.generate()}/>
        })}
      </div>
    );
  }
}