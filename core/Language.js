const LanguageConstants = {
  HAS_PROPERTIES: 'has the properties'
};

let words = [
  {
    test: /^@$/,
    type: 'prefix.collection',
    docs: '<div>creates a collection</div>'
  },
  {
    test: /@[a-z]/i,
    type: 'symbol.collection',
    next: [
      LanguageConstants.HAS_PROPERTIES
    ]
  },
  {
    name: LanguageConstants.HAS_PROPERTIES,
    test: /has the properties/,
    type: 'ownership.collection.properties',
    docs: '<div>create comma separated properties</div>'
  }
];

export default class Language {
  static findWord(string) {
    let wordCount = words.length;

    for (let i = 0; i < wordCount; i++) {
      let word = words[i];

      if (word.test.test(string)) {
        return word;
      }
    }
  }
}