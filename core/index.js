'use strict';

window.onload = () => {
  require('./index.css');

  let AuraWindow = require('./components/window/AuraWindow.react.js');

  React.render(<AuraWindow />, document.body);
};