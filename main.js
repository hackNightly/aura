'use strict';

var app           = require('app');
var BrowserWindow = require('browser-window');

require('crash-reporter').start();

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is GCed.
var mainWindow = null;

app.on('window-all-closed', function() {
  process.platform === 'darwin' || app.quit();
});

app.on('ready', function() {
  mainWindow = new BrowserWindow({width: 1500, height: 960, frame: false, x:2100, y: 100});

  mainWindow.loadUrl(`file://${__dirname}/index.html`);

  mainWindow.on('closed', function() {
    mainWindow = null;
  });
});