'use strict';

import { Dispatcher } from 'flux';

let AuraDispatcher = new Dispatcher();

AuraDispatcher.handleViewAction = (action) => {
  AuraDispatcher.dispatch({
    source: 'VIEW_ACTION',
    action: action
  });
};

export {AuraDispatcher};
