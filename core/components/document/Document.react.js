require('./document.css');

import Statement from './Statement.react.js';

class Document extends React.Component {
  constructor(props) {
    super(props);

    this.document = this.props.initialDocument;
  }

  componentDidMount() {
    let elem = React.findDOMNode(this);
  }

  render() {
    if (!this.document.statements.length) {
      return (
        <div className='aura-document'>
          <Statement />
        </div>
      );
    }
  }
}

export {Document}