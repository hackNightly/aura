let shortid = require('shortid');

import Language from '../../Language.js';

export default class Statement extends React.Component {
  constructor(props) {
    super(props);

    this.input = null;
    this.$input = null;
  }

  componentDidMount() {
    let elem = React.findDOMNode(this);

    this.input = elem.querySelector('input');

    this.$input = new Awesomplete(this.input, {
      list: [],
      minChars: 0
    });

    this.input.onkeyup = (e) => {
      let value = this.input.value;
      let word  = Language.findWord(value);

      console.log(word);
    };
  }

  render() {
    let listId = shortid.generate();

    return (
      <div className='aura-statement'>
        <input key={shortid.generate()}/>
      </div>
    )
  }
}