var precss = require('precss');

module.exports = {
  entry: './core/index.js',
  output: {
    path: __dirname + '/dist',
    publicPath: '/assets/'
  },
  module: {
    loaders: [
      {test: /\.js$/, loader: 'babel'},
      {test: /\.css$/, loader: 'style-loader!css-loader!postcss-loader'}
    ]
  },
  externals: {
    'react': 'React',
    'ace': 'ace'
  },
  postcss: function() {
    return [precss]
  }
};