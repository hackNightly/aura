'use strict';

require('./menubar.css');

let MenuBar = require('./MenuBar.react.js');
let Editor  = require('../editor/Editor.react.js');

export default class AuraWindow extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div id='aura-root' className='aura-light'>
        <MenuBar />
        <Editor />
      </div>
    );
  }
}